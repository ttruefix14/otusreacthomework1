import React from 'react';
import logo from './logo.svg';
import './App.scss';
import { ContainerClass } from './Components/Container/ContainerClass';
import { ContainerFunc } from './Components/Container/ContainerFunc';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Домашнее задание по React № 1
        </p>
      </header>
      <ContainerClass></ContainerClass>
      <ContainerFunc></ContainerFunc>
    </div>
  );
}

export default App;
