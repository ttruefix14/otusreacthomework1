import { PureComponent } from 'react';
import './Response.scss';
import { ResponseData } from '../Container/ContainerClass';


interface ResponseProps {
    responseData: ResponseData;
}

export class ResponseClass extends PureComponent<ResponseProps> {
    render() {
        let responseClass: string;
        if (this.props.responseData.error) {
            responseClass = "response fail";
        }
        else {
            responseClass = "response";
        }

        return <div className={responseClass}>
            <p>{this.props.responseData.text || "Здесь будет полученная информация..."}</p>
        </div>;
    }
}