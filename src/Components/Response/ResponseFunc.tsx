import './Response.scss';
import { ResponseData } from '../Container/ContainerClass';


interface ResponseProps {
    responseData: ResponseData;
}

export function ResponseFunc(props: ResponseProps) {

    let responseClass: string;
    if (props.responseData.error) {
        responseClass = "response fail";
    }
    else {
        responseClass = "response";
    }

    return <div className={responseClass}>
        <p>{props.responseData.text || "Здесь будет полученная информация..."}</p>
    </div>;
}