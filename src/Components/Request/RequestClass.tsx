import { PureComponent } from 'react';
import './Request.scss';

import { Button } from 'react-bootstrap';
import axios from 'axios';
import { ResponseData } from '../Container/ContainerClass';


interface RequestProps {
    onChangeResponse: (value: ResponseData) => void;
}

export class RequestClass extends PureComponent<RequestProps> {
    constructor(props: any) {
        super(props)
        this.updateInput = this.updateInput.bind(this);
        this.onClick = this.onClick.bind(this);

    }

    url?: string;

    updateInput(event: any) {
        this.url = event.target.value;
    }

    onClick = () => {
        let responseData = new ResponseData();

        if (!this.url) {
            this.props.onChangeResponse(responseData);
        }
        else {
            axios.get(this.url)
                .then((response) => {
                    if (response.data.error) {
                        throw new Error(response.data.error)
                    }
                    if (!response.headers["content-type"].includes("application/json")) {
                        throw new Error(`Некорректный тип данных: "${response.headers["content-type"]}"`)
                    }

                    responseData.text = JSON.stringify(response.data);
                    responseData.error = false;
                    this.props.onChangeResponse(responseData);
                })
                .catch((error) => {
                    responseData.text = error.message;
                    responseData.error = true;
                    this.props.onChangeResponse(responseData);
                })

        }
    }

    render() {
        return <div className="request">
            <input onChange={this.updateInput}></input>
            <Button variant="primary" className="btn-primary" onClick={this.onClick}>Отправить</Button>
        </div>
    }
}