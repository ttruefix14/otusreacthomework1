import './Request.scss';

import { Button } from 'react-bootstrap';
import axios from 'axios';
import { ResponseData } from '../Container/ContainerClass';
import { useState } from 'react';

interface RequestProps {
    onChangeResponse: (value: ResponseData) => void;
}


export function RequestFunc(props: RequestProps) {
    let [url, setUrl] = useState<string>();

    let updateInput = (event: any) => {
        setUrl(event.target.value);
    }

    let onClick = () => {
        let responseData = new ResponseData();

        if (!url) {
            props.onChangeResponse(responseData);
        }
        else {
            axios.get(url)
                .then((response) => {
                    if (response.data.error) {
                        throw new Error(response.data.error)
                    }

                    if (!response.headers["content-type"].includes("application/json")) {
                        throw new Error(`Некорректный тип данных: "${response.headers["content-type"]}"`)
                    }

                    responseData.text = JSON.stringify(response.data);
                    responseData.error = false;
                    props.onChangeResponse(responseData);
                })
                .catch((error) => {
                    responseData.text = error.message;
                    responseData.error = true;
                    props.onChangeResponse(responseData);
                })

        }
    }

    return <div className="request">
        <input onChange={updateInput}></input>
        <Button variant="primary" className="btn-primary" onClick={onClick}>Отправить</Button>
    </div>
}