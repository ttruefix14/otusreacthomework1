import './Container.scss';

import { PropsWithChildren, useState } from 'react';

import { RequestFunc } from '../Request/RequestFunc';
import { ResponseFunc } from '../Response/ResponseFunc';

export class ResponseData {
    text?: string;
    error: boolean = false;
}

export function ContainerFunc(props: PropsWithChildren) {
    let [response, setResponse] = useState<ResponseData>(new ResponseData());

    let onChangeResponse = (value: ResponseData) => {
        setResponse(value);
    }

    return <div className="container">
        <h1>Функциональный компонент</h1>
        <p>Запросить информацию из публичного API</p>
        <RequestFunc onChangeResponse={onChangeResponse}></RequestFunc>
        <ResponseFunc responseData={response}></ResponseFunc>
        <span>{new Date().toLocaleString()}</span>
    </div>

}