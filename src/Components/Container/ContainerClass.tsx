import './Container.scss';

import { Component, PropsWithChildren } from 'react';

import { RequestClass } from '../Request/RequestClass';
import { ResponseClass } from '../Response/ResponseClass';

export class ResponseData {
    text?: string;
    error: boolean = false;
}

interface ContainerState {
    response: ResponseData;
}

export class ContainerClass extends Component<PropsWithChildren, ContainerState> {
    constructor(props: any) {
        super(props);
        this.state = { response: new ResponseData() }
        this.onChangeResponse = this.onChangeResponse.bind(this);
    }

    onChangeResponse(value: ResponseData) {
        this.setState({ response: value })
    }

    render() {
        return <div className="container">
            <h1>Компонент на классе</h1>
            <p>Запросить информацию из публичного API</p>
            <RequestClass onChangeResponse={this.onChangeResponse}></RequestClass>
            <ResponseClass responseData={this.state.response}></ResponseClass>
            <span>{new Date().toLocaleString()}</span>
        </div>
    }
}